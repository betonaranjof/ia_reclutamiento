package mx.beto.model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import mx.beto.reclutamiento.R;

public class MyRecyclerAdapterTransactions  extends RecyclerView.Adapter<MyRecyclerAdapterTransactions.MyViewHolder> {

    private List<Transacciones> transaccionesList;
    private Context context;
    private LayoutInflater inflater;

    public MyRecyclerAdapterTransactions(Context context, List<Transacciones> transaccionesList) {

        this.context = context;
        this.transaccionesList = transaccionesList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View rootView = inflater.inflate(R.layout.item_transaction, viewGroup, false);
        return new MyViewHolder(rootView);
    }//onCreateViewHolder

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Transacciones transacciones = transaccionesList.get(position);
        holder.tran_cinema.setText(transacciones.getCinema());
        holder.tran_message.setText(transacciones.getMessage());
        holder.ttran_date.setText(transacciones.getDate());
        holder.tran_points.setText(String.valueOf(transacciones.getPoints()));
    }

    @Override
    public int getItemCount() {
        return transaccionesList.size();
    }//getItemCount

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tran_cinema, tran_message, ttran_date, tran_points;

        public MyViewHolder(View itemView) {
            super(itemView);
            tran_cinema = itemView.findViewById(R.id.tran_cinema);
            tran_message = itemView.findViewById(R.id.tran_message);
            ttran_date = itemView.findViewById(R.id.ttran_date);
            tran_points = itemView.findViewById(R.id.tran_points);


        }//MyViewHolder
    }//MyViewHolder
}
