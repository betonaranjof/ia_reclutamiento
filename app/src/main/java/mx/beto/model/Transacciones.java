package mx.beto.model;

import java.io.Serializable;

public class Transacciones implements Serializable {
    String cinema, message, date;
    Double points;

    public Transacciones() {
    }

    public Transacciones(String cinema, String message, String date, Double points) {
        this.cinema = cinema;
        this.message = message;
        this.date = date;
        this.points = points;
    }

    public String getCinema() {
        return cinema;
    }

    public void setCinema(String cinema) {
        this.cinema = cinema;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }
}//Main
