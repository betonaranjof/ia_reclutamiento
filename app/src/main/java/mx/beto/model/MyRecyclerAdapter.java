package mx.beto.model;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import mx.beto.reclutamiento.R;
import mx.beto.reclutamiento.fragmentos.DetallePelicula;
import mx.beto.reclutamiento.fragmentos.UserProfile;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {

    private List<Pelicula> peliculaList;
    private Context context;
    private LayoutInflater inflater;

    public MyRecyclerAdapter(Context context, List<Pelicula> peliculaList) {

        this.context = context;
        this.peliculaList = peliculaList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View rootView = inflater.inflate(R.layout.item_cartelera, viewGroup, false);
        return new MyViewHolder(rootView);
    }//onCreateViewHolder

    @Override
    public void onBindViewHolder(@NonNull MyRecyclerAdapter.MyViewHolder holder, int position) {
        Pelicula pelicula = peliculaList.get(position);
        holder.title_movie.setText(pelicula.getNombre());
        Picasso.get()
                .load(pelicula.getCartel())
                .error(R.drawable.file_error)
                .into(holder.imageview);
        holder.itemView.setOnClickListener(v -> {

            Bundle bundle=new Bundle();
            bundle.putSerializable("pelicula", pelicula);

            AppCompatActivity activity = (AppCompatActivity) v.getContext();
            Fragment myFragment = new DetallePelicula();
            myFragment.setArguments(bundle);
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_fragment_containers, myFragment)
                    .addToBackStack(null)
                    .commit();

        });
    }//onBindViewHolder

    @Override
    public int getItemCount() {
        return peliculaList.size();
    }//getItemCount

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView title_movie;
        private ImageView imageview;

        public MyViewHolder(View itemView) {
            super(itemView);
            title_movie = itemView.findViewById(R.id.title_movie);
            imageview = itemView.findViewById(R.id.thumbnail);


        }//MyViewHolder
    }//MyViewHolder
}
