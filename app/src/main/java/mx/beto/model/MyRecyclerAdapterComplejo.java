package mx.beto.model;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import mx.beto.reclutamiento.R;
import mx.beto.reclutamiento.fragmentos.DetalleComplejo;
import mx.beto.reclutamiento.fragmentos.DetallePelicula;

public class MyRecyclerAdapterComplejo extends RecyclerView.Adapter<MyRecyclerAdapterComplejo.MyViewHolder>  {

    private List<Complejo> complejoList;
    private Context context;
    private LayoutInflater inflater;

    public MyRecyclerAdapterComplejo(Context context, List<Complejo> complejoList) {

        this.context = context;
        this.complejoList = complejoList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View rootView = inflater.inflate(R.layout.item_complejo, viewGroup, false);
        return new MyViewHolder(rootView);
    }//onCreateViewHolder

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Complejo complejo = complejoList.get(position);
        holder.complejo_name.setText(complejo.getName());
        holder.complejo_phone.setText(complejo.getPhone());
        holder.complejo_address.setText(complejo.getAddress());
        holder.itemView.setOnClickListener(v -> {

            Bundle bundle=new Bundle();
            bundle.putSerializable("complejo", complejo);

            AppCompatActivity activity = (AppCompatActivity) v.getContext();
            Fragment myFragment = new DetalleComplejo();
            myFragment.setArguments(bundle);
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_fragment_containers, myFragment)
                    .addToBackStack(null)
                    .commit();

        });
    }

    @Override
    public int getItemCount() {
        return complejoList.size();
    }//getItemCount

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView complejo_name, complejo_phone, complejo_address;

        public MyViewHolder(View itemView) {
            super(itemView);
            complejo_name = itemView.findViewById(R.id.complejo_name);
            complejo_phone = itemView.findViewById(R.id.complejo_phone);
            complejo_address = itemView.findViewById(R.id.complejo_address);


        }//MyViewHolder
    }//MyViewHolder

}
