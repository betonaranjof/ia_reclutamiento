package mx.beto.model;

import java.io.Serializable;

public class Pelicula implements Serializable {

    public String trailer;
    public String nombre;
    public String clasificacion;
    public String genero;
    public String duracion;
    public String sinopsis;
    public String cartel;

    public Pelicula() {
    }

    public Pelicula(String nombre, String clasificacion, String genero, String duracion, String sinopsis, String trailer, String cartel) {
        this.nombre = nombre;
        this.clasificacion = clasificacion;
        this.genero = genero;
        this.duracion = duracion;
        this.sinopsis = sinopsis;
        this.trailer = trailer;
        this.cartel = cartel;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getCartel() {
        return cartel;
    }

    public void setCartel(String cartel) {
        this.cartel = cartel;
    }
}//Main
