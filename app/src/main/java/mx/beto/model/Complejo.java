package mx.beto.model;

import java.io.Serializable;

public class Complejo implements Serializable {
    String name, lat, lng, phone, address;

    public Complejo() {
    }

    public Complejo(String name, String lat, String lng, String phone, String address) {
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.phone = phone;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}//Main
