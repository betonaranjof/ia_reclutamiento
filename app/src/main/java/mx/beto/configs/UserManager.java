package mx.beto.configs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import mx.beto.reclutamiento.SplashScreen;

public class UserManager {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    private int PRIVATE_MODE =0;

    private static final String PREFER_NAME = "USER_PREFERENCES";
    private static final String CORREO = "CORREO";
    private static final String NOMBRE = "NOMBRE";
    private static final String APELLIDOS = "APELLIDOS";
    private static final String TELEFONO = "CORRTELEFONOEO";
    private static final String IMG = "IMG";
    private static final String NO_TARJETA = "NO_TARJETA";
    private JSONObject varUser;

    public UserManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME,PRIVATE_MODE);
        editor = pref.edit();
    }//Constructor

    public void createSessionProfile(
            String correo,
            String nombre,
            String apellidos,
            String telefono,
            String img,
            String no_tarjeta
    ){
        editor.putString(CORREO, correo);
        editor.putString(NOMBRE, nombre);
        editor.putString(APELLIDOS, apellidos);
        editor.putString(TELEFONO, telefono);
        editor.putString(IMG, img);
        editor.putString(NO_TARJETA, no_tarjeta);
        editor.commit();
    }//createSessionPatrol

    public JSONObject getUserCredentials(){
        try{
            varUser = new JSONObject();
            varUser.put(CORREO, pref.getString(CORREO, null));
            varUser.put(NOMBRE, pref.getString(NOMBRE, null));
            varUser.put(APELLIDOS, pref.getString(APELLIDOS, null));
            varUser.put(TELEFONO, pref.getString(TELEFONO, null));
            varUser.put(IMG, pref.getString(IMG, null));
            varUser.put(NO_TARJETA, pref.getString(NO_TARJETA, null));
        }catch(JSONException je){
            je.printStackTrace();
        }
        return varUser;
    }//getPatrolDetails


    public void borraVariables(){
        editor.clear();
        editor.commit();
    }
    public void borraVariablesReinicia(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, SplashScreen.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

}
