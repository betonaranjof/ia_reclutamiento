package mx.beto.configs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;

import mx.beto.reclutamiento.SplashScreen;

public class SessionManager {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    private int PRIVATE_MODE =0;

    private static final String PREFER_NAME = "Reclutamiento";
    private static final String USERNAME = "USERNAME";
    private static final String PASSWORD = "PASSWORD";
    private static final String TOKEN_ID = "TOKEN_ID";
    private static final String TOKEN_TYPE = "TOKEN_TYPE";
    private JSONObject variablesJSON;

    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME,PRIVATE_MODE);
        editor = pref.edit();
    }//Constructor

    public void createSessionPatrol(
            String username,
            String password,
            String tokenId,
            String tokenType
    ){
        editor.putString(USERNAME, username);
        editor.putString(PASSWORD, password);
        editor.putString(TOKEN_ID, tokenId);
        editor.putString(TOKEN_TYPE, tokenType);
        editor.commit();
    }//createSessionPatrol

    public void setToken(String token){
        editor.putString(TOKEN_ID, token);
        editor.commit();
    }//setImei


    public JSONObject getUserCredentials(){
        try{
            variablesJSON = new JSONObject();
            variablesJSON.put(TOKEN_ID, pref.getString(TOKEN_ID, null));
            variablesJSON.put(TOKEN_TYPE, pref.getString(TOKEN_TYPE, null));
            variablesJSON.put(USERNAME, pref.getString(USERNAME, null));
            variablesJSON.put(PASSWORD, pref.getString(PASSWORD, null));
        }catch(JSONException je){
            je.printStackTrace();
        }
        return variablesJSON;
    }//getPatrolDetails


    public void borraVariables(){
        editor.clear();
        editor.commit();
    }
    public void borraVariablesReinicia(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, SplashScreen.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

}//Main
