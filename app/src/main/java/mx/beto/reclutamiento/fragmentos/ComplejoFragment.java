package mx.beto.reclutamiento.fragmentos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.beto.model.Complejo;
import mx.beto.model.MyRecyclerAdapterComplejo;
import mx.beto.model.Pelicula;
import mx.beto.reclutamiento.R;

public class ComplejoFragment extends Fragment {

    public View rootView;
    public RecyclerView recyclerView;
    public List<Complejo> complejoList = new ArrayList<>();
    public MyRecyclerAdapterComplejo adapter;
    public ProgressBar progressbar_complejo;
    public JSONObject respuesta;
    public Complejo complejo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }//onCreate

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.complejo_activity, container, false);
        initElements();
        return rootView;
    }//onCreateView

    public void initElements(){
        //Initialize RecyclerView
        recyclerView = rootView.findViewById(R.id.rv_complejos);
        adapter = new MyRecyclerAdapterComplejo(getContext(), complejoList);
        progressbar_complejo = rootView.findViewById(R.id.progressbar_complejo);
        progressbar_complejo.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        requestComplejos();

    }//initELements

    public void requestComplejos(){
        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(getActivity().getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET,
                "https://stage-api.cinepolis.com/v2/locations/cinemas/69?country_code=MX",
                response -> {
                    try{
                        respuesta = new JSONObject(response);
                        complejo = new Complejo(
                                respuesta.getString("name"),
                                respuesta.getString("lat"),
                                respuesta.getString("lng"),
                                respuesta.getString("phone"),
                                respuesta.getString("address")
                        );
                        complejoList.add(complejo);
                        //para cuando se suban más complejos en array, recorrer y mandar al adapter los objetos
                        adapter.notifyItemChanged(1);
                        Log.e("complejos", respuesta.toString());
                    }catch (JSONException je){
                        je.printStackTrace();
                    }//JSONException
                    progressbar_complejo.setVisibility(View.GONE);
                },
                error -> {
                    progressbar_complejo.setVisibility(View.GONE);
                    Log.e("VOLLEY Error", error.toString());
                    try {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            Log.e("Error", "Error");
                        }
                        String flag = String.valueOf(networkResponse.statusCode);
                        if (flag == null || flag.isEmpty()) {
                            Log.e("Flag", flag);
                        } else {
                            Log.e("statusCode Error", "" + networkResponse.statusCode);
                            Log.e("Flag", "" + networkResponse.statusCode);
                        }
                    } catch (NullPointerException npe) {
                        npe.printStackTrace();
                        Log.e("NPE", npe.getMessage());
                    }
                }//errorListener
        ) {
            /**
             @Override
             protected Map<String, String> getParams() {
             Map<String, String> params = new HashMap<>();
             params.put("country_code", "MX");
             params.put("cinemas", "32");
             return params;
             }
             */
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("api_key", "stage_HNYh3RaK_Test");
                return params;
            }
        };
        mRequestQueue.add(jsonObjectRequest);
    }//requestComplejos


}
