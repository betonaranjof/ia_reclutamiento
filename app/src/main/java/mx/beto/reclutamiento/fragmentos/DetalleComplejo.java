package mx.beto.reclutamiento.fragmentos;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import mx.beto.model.Complejo;
import mx.beto.reclutamiento.R;

public class DetalleComplejo extends Fragment implements OnMapReadyCallback {

    public View rootView;
    public Complejo complejo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }//onCreate

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.detalle_complejo, container, false);
        complejo = (Complejo) getArguments().getSerializable("complejo");
        initElements();
        return rootView;
    }//onCreateView

    public void initElements(){
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }//initElements

    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng complejoPos = new LatLng(Float.parseFloat(complejo.getLat()), Float.parseFloat(complejo.getLng()));
        googleMap.addMarker(new MarkerOptions().position(complejoPos)
                .title(complejo.getName()));
        //googleMap.moveCamera(CameraUpdateFactory.newLatLng(complejoPos));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(complejoPos, 13));

    }//onMapReady
}
