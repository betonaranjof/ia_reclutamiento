package mx.beto.reclutamiento.fragmentos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.beto.configs.SessionManager;
import mx.beto.configs.UserManager;
import mx.beto.reclutamiento.MainActivity;
import mx.beto.reclutamiento.R;

public class UserProfile extends Fragment implements View.OnClickListener {

    public TextView email_response, fname_response, lname_response,
    phone_response, picture_response, card_response;
    public SessionManager sessionManager;
    public UserManager userManager;
    public ProgressBar progressbar_perfil;
    public String tokenType, tokenId;
    public JSONObject jsonObject;
    public View rootView;
    public Button btn_logout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }//onCreate

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.perfil_usuario_activity,container,false);
        sessionManager = new SessionManager(getContext());
        userManager = new UserManager(getContext());
        initElements();
        return rootView;
    }//onCreateView

    public void initElements(){
        progressbar_perfil = rootView.findViewById(R.id.progressbar_perfil);
        progressbar_perfil.setVisibility(View.VISIBLE);
        email_response = rootView.findViewById(R.id.email_response);
        fname_response = rootView.findViewById(R.id.fname_response);
        lname_response = rootView.findViewById(R.id.lname_response);
        phone_response = rootView.findViewById(R.id.phone_response);
        picture_response = rootView.findViewById(R.id.picture_response);
        btn_logout = rootView.findViewById(R.id.btn_logout);
        card_response = rootView.findViewById(R.id.card_response);
        try {
            tokenId = sessionManager.getUserCredentials().getString("TOKEN_ID");
            tokenType = sessionManager.getUserCredentials().getString("TOKEN_TYPE");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestProfile(tokenId, tokenType);
        btn_logout.setOnClickListener(this);
    }//initElements

    public void requestProfile(String tokenId, String tokenType) {

        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(getActivity().getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET,
                "https://stage-api.cinepolis.com/v1/members/profile?country_code=MX",
                response -> {
                    manageResponse(response);
                },
                error -> {
                    progressbar_perfil.setVisibility(View.GONE);
                    Log.e("VOLLEY Error", error.toString());
                    try {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            Log.e("Error", "Error");
                        }
                        String flag = String.valueOf(networkResponse.statusCode);
                        if (flag == null || flag.isEmpty()) {
                            Log.e("Flag", flag);
                        } else {
                            Log.e("statusCode Error", "" + networkResponse.statusCode);
                            Log.e("Flag", "" + networkResponse.statusCode);
                        }
                    } catch (NullPointerException npe) {
                        npe.printStackTrace();
                        Log.e("NPE", npe.getMessage());
                    }
                }//errorListener
        ) {
            /**
             * Credenciales:
             * pruebas_beto_ia@yahoo.com
             * Pruebas01
             * */
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("api_key", "stage_HNYh3RaK_Test");
                params.put("Authorization", tokenType+" "+tokenId);
                return params;
            }
        };
        mRequestQueue.add(jsonObjectRequest);
    }//requestLogin

    public void manageResponse(String response){
        Log.e("Response", response);
        try{
            jsonObject = new JSONObject(response);
            email_response.setText(jsonObject.getString("email"));
            fname_response.setText(jsonObject.getString("first_name"));
            lname_response.setText(jsonObject.getString("last_name"));
            phone_response.setText(jsonObject.getString("phone_number"));
            picture_response.setText(jsonObject.getString("profile_picture"));
            card_response.setText(jsonObject.getString("card_number"));
            userManager.createSessionProfile(
                    jsonObject.getString("email"),
                    jsonObject.getString("first_name"),
                    jsonObject.getString("last_name"),
                    jsonObject.getString("phone_number"),
                    jsonObject.getString("profile_picture"),
                    jsonObject.getString("card_number"));

        }catch (JSONException je){
            je.printStackTrace();
        }
        progressbar_perfil.setVisibility(View.GONE);
    }//manageResponse

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_logout:
                sessionManager.borraVariablesReinicia();
                break;
        }
    }
}//Main
