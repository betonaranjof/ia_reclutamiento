package mx.beto.reclutamiento.fragmentos;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.beto.model.MyRecyclerAdapter;
import mx.beto.model.Pelicula;
import mx.beto.reclutamiento.R;

public class Cartelera extends Fragment {

    public View rootView;
    public RecyclerView recyclerView;
    public List<Pelicula> peliculaList = new ArrayList<>();
    public MyRecyclerAdapter adapter;
    public ProgressBar progressbar_cartelera;

    public String uriPoster, uriTrailer;
    public JSONObject respuesta;
    public JSONArray movies, routes;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }//onCreate

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.cartelera_activity, container, false);
        initElements();
        return rootView;
    }//onCreateView

    public void initElements(){
        //Initialize RecyclerView
        recyclerView = rootView.findViewById(R.id.rv_cartelera);
        adapter = new MyRecyclerAdapter(getContext(), peliculaList);
        progressbar_cartelera = rootView.findViewById(R.id.progressbar_cartelera);
        progressbar_cartelera.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        requestCartel();

    }//initELements

    public void requestCartel() {

        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(getActivity().getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET,
                "https://stage-api.cinepolis.com/v2/movies?country_code=MX&cinemas=32",
                response -> {
                    try{
                        respuesta = new JSONObject(response);
                        movies = new JSONArray();
                        routes = new JSONArray();
                        movies = respuesta.getJSONArray("movies");
                        routes = respuesta.getJSONArray("routes");
                        for(int i = 0; i < routes.length(); i++) {
                            JSONObject a = routes.getJSONObject(i);
                            if (a.getString("code").equals("poster")){
                                JSONObject size = a.getJSONObject("sizes");
                                uriPoster = size.getString("large").replace("http", "https");
                            }else if (a.getString("code").equals("trailer_mp4")){
                                JSONObject size = a.getJSONObject("sizes");
                                uriTrailer = size.getString("medium");
                            }
                        }//for url

                        for(int i = 0; i < movies.length(); i++) {
                            JSONObject a = movies.getJSONObject(i);
                            JSONArray media = a.getJSONArray("media");
                            String poster = "", trailer = "";
                            for(int m = 0; m < media.length(); m++){
                                JSONObject med = media.getJSONObject(m);
                                if (med.getString("code").equals("poster")){
                                    poster = med.getString("resource");
                                }else if (med.getString("code").equals("trailer_mp4")){
                                    trailer = med.getString("resource");
                                }
                            }//name medias
                            Pelicula pelicula = new Pelicula(
                                    a.getString("name"),
                                    a.getString("rating"),
                                    a.getString("genre"),
                                    a.getString("length"),
                                    a.getString("synopsis"),uriTrailer + trailer, uriPoster + poster
                            );
                            peliculaList.add(pelicula);
                            adapter.notifyItemChanged(i);
                            Log.e("poster", uriPoster + poster);
                        }//for movies


                    }catch (JSONException je){
                        je.printStackTrace();
                    }//JSONException
                    progressbar_cartelera.setVisibility(View.GONE);
                },
                error -> {
                    progressbar_cartelera.setVisibility(View.GONE);
                    Log.e("VOLLEY Error", error.toString());
                    try {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            Log.e("Error", "Error");
                        }
                        String flag = String.valueOf(networkResponse.statusCode);
                        if (flag == null || flag.isEmpty()) {
                            Log.e("Flag", flag);
                        } else {
                            Log.e("statusCode Error", "" + networkResponse.statusCode);
                            Log.e("Flag", "" + networkResponse.statusCode);
                        }
                    } catch (NullPointerException npe) {
                        npe.printStackTrace();
                        Log.e("NPE", npe.getMessage());
                    }
                }//errorListener
        ) {
            /**
             @Override
             protected Map<String, String> getParams() {
             Map<String, String> params = new HashMap<>();
             params.put("country_code", "MX");
             params.put("cinemas", "32");
             return params;
             }
             */
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("api_key", "stage_HNYh3RaK_Test");
                return params;
            }
        };
        mRequestQueue.add(jsonObjectRequest);
    }//requestCartel

}//Main
