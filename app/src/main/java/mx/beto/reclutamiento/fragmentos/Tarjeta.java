package mx.beto.reclutamiento.fragmentos;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.crowdfire.cfalertdialog.CFAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import mx.beto.configs.SessionManager;
import mx.beto.configs.UserManager;
import mx.beto.model.MyRecyclerAdapterTransactions;
import mx.beto.model.Transacciones;
import mx.beto.reclutamiento.R;

public class Tarjeta extends Fragment implements View.OnClickListener {

    public View rootView;
    public SessionManager sessionManager;
    public UserManager userManager;
    public ProgressBar progresbar_card;
    public EditText et_card_club;
    public Button btn_verify_card;
    public String noCard;
    public JSONArray transactons;
    public RecyclerView rv_transacciones;
    public List<Transacciones> transaccionesList = new ArrayList<>();
    public MyRecyclerAdapterTransactions adapter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }//onCreate

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.tarjeta_detalle,container,false);
        sessionManager = new SessionManager(getContext());
        userManager = new UserManager(getContext());
        initElements();
        return rootView;
    }//onCreateView

    public void initElements(){
        rv_transacciones = rootView.findViewById(R.id.rv_transacciones);
        adapter = new MyRecyclerAdapterTransactions(getContext(), transaccionesList);

        progresbar_card = rootView.findViewById(R.id.progresbar_card);
        et_card_club = rootView.findViewById(R.id.et_card_club);
        btn_verify_card = rootView.findViewById(R.id.btn_verify_card);
        progresbar_card.setVisibility(View.GONE);
        btn_verify_card.setOnClickListener(this);
        rv_transacciones.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_transacciones.setAdapter(adapter);
    }//initElements

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_verify_card:
                noCard = et_card_club.getText().toString().trim();
                if (noCard == null || noCard.isEmpty()){
                    mensajeAlerta(
                            getResources().getString(R.string.card_verify_msj_title),
                            getResources().getString(R.string.card_verify_msj_body)
                    );
                }else{
                    progresbar_card.setVisibility(View.VISIBLE);
                    Log.e("Vamo'", "a verirficar");
                    try {
                        String tType = sessionManager.getUserCredentials().getString("TOKEN_TYPE");
                        String tID = sessionManager.getUserCredentials().getString("TOKEN_ID");
                        Log.e("token_type", tType);
                        Log.e("token_id", tID);
                        requestRecords(noCard, tType, tID);
                    }catch (JSONException je){
                        je.printStackTrace();
                    }
                }
                break;
        }//Switch
    }//onClick

    public void requestRecords(String noCard, String tokenType, String tokenId) {

        JSONObject jo = new JSONObject();
        try{
            jo.put("country_code", "MX");
            jo.put("card_number", noCard);
            jo.put("transaction_include", true);
            jo.put("pin", "5717");
        }catch (JSONException jee){
            jee.printStackTrace();
        }

        Log.e("JO", jo.toString());

        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(getActivity().getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                "https://stage-api.cinepolis.com/v2/members/loyalty/",
                        jo,
                        response -> {
                    try{
                        transactons = new JSONArray();
                        transactons = response.getJSONArray("transactions");

                        for(int i = 0; i < transactons.length(); i++) {
                            JSONObject a = transactons.getJSONObject(i);
                            Transacciones transaccion = new Transacciones(
                                    a.getString("cinema"),
                                    a.getString("message"),
                                    a.getString("date"),
                                    a.getDouble("points")
                            );
                            transaccionesList.add(transaccion);
                            adapter.notifyItemChanged(i);
                        }//for movies

                        Log.e("respuesta", response.toString());
                        progresbar_card.setVisibility(View.GONE);
                    }catch (JSONException je){
                        je.printStackTrace();
                    }//try-catch

                        },
                        error -> {
                            progresbar_card.setVisibility(View.GONE);
                            mensajeAlerta("Error", "Verifique sus cuentas.");
                            Log.e("VOLLEY Error", error.toString());
                            try {
                                NetworkResponse networkResponse = error.networkResponse;
                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    Log.e("Error", "Error");
                                }
                                String flag = String.valueOf(networkResponse.statusCode);
                                if (flag == null || flag.isEmpty()) {
                                    Log.e("Flag", flag);
                                } else {
                                    Log.e("statusCode Error", "" + networkResponse.statusCode);
                                    Log.e("Flag", "" + networkResponse.statusCode);
                                }
                            } catch (NullPointerException npe) {
                                npe.printStackTrace();
                                Log.e("NPE", npe.getMessage());
                            }
                        }//errorListener
                ) {
/**
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("country_code", "MX");
                params.put("card_number", String.valueOf(noCard));
                params.put("pin", "5717");
                params.put("transaction_include", "true");
                return params;
            }
*/
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("api_key", "stage_HNYh3RaK_Test");
                params.put("Authorization", tokenType+" "+tokenId);
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("Connection", "close");
                return params;
            }
        };
        mRequestQueue.add(jsonObjectRequest);

    }//requestLogin

    public void mensajeAlerta(String titulo, String mensaje) {
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(getContext())
                .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                .setTitle(titulo)
                .setMessage(mensaje)
                .setTextGravity(Gravity.CENTER_HORIZONTAL)
                .setCancelable(false)
                .addButton("CERRAR", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE,
                        CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, (dialog, which) -> {
                            dialog.dismiss();
                        });

// Show the alert
        builder.show();
    }//MensajeAlerta
}//Main
