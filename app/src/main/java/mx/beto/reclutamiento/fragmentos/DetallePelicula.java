package mx.beto.reclutamiento.fragmentos;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import mx.beto.model.Pelicula;
import mx.beto.reclutamiento.R;

public class DetallePelicula extends Fragment {

    public View rootView;
    public Pelicula pelicula;
    VideoView videoView;
    public TextView tv_movie_name, tv_movie_rating, tv_movie_genre,
            tv_movie_length, tv_movie_synopsis;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }//onCreate

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.detalle_pelicula, container, false);
        pelicula = (Pelicula) getArguments().getSerializable("pelicula");
        Log.e("Message", pelicula.getNombre());
        initElements();
        return rootView;
    }

    public void initElements(){

        tv_movie_name = rootView.findViewById(R.id.tv_movie_name);
        tv_movie_rating = rootView.findViewById(R.id.tv_movie_rating);
        tv_movie_genre = rootView.findViewById(R.id.tv_movie_genre);
        tv_movie_length = rootView.findViewById(R.id.tv_movie_length);
        tv_movie_synopsis = rootView.findViewById(R.id.tv_movie_synopsis);

        videoView = rootView.findViewById(R.id.trailer_pelicula);
        final MediaController mediacontroller = new MediaController(getContext());
        mediacontroller.setAnchorView(videoView);

        videoView.setMediaController(mediacontroller);
        videoView.setVideoURI(Uri.parse(pelicula.getTrailer().replace("http", "https")));
        videoView.start();
        //videoView.setVideoURI(Uri.parse("https://movil.cinepolis.com/android/trailer/venganza.mp4"));
        videoView.requestFocus();
        videoView.setOnCompletionListener(mp -> Toast.makeText(getContext(), "Video over", Toast.LENGTH_SHORT).show());

        videoView.setOnErrorListener((mp, what, extra) -> {
            Log.d("API123", "What " + what + " extra " + extra);
            return false;
        });
        tv_movie_name.setText(pelicula.getNombre());
        tv_movie_rating.setText(pelicula.getClasificacion());
        tv_movie_genre.setText(pelicula.getGenero());
        tv_movie_length.setText(pelicula.getDuracion());
        tv_movie_synopsis.setText(pelicula.getSinopsis());


    }//initELements
}//Main
