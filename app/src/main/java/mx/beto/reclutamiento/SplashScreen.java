package mx.beto.reclutamiento;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.wangyuwei.particleview.ParticleView;
import mx.beto.configs.SessionManager;

public class SplashScreen extends Activity {

    //VARIABLES
    private static final long SPLASH_SCREEN_DELAY = 3000;
    String[] perms = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET
    };//permisos
    private ParticleView mPv1;
    public SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash_screen);
        sessionManager = new SessionManager(this);
        initElements();
    }//onCreate

    public void initElements(){
        mPv1 = findViewById(R.id.pv_1);

        mPv1.setOnParticleAnimListener(new ParticleView.ParticleAnimListener() {
            @Override
            public void onAnimationEnd() {
                permisos();
            }
        });//ParticleListener

        mPv1.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPv1.startAnim();
            }
        },SPLASH_SCREEN_DELAY);
    }//initElements

    public void permisos() {
        Permissions.check(SplashScreen.this/*context*/, perms, null, null, new PermissionHandler() {
            @Override
            public void onGranted() {
                // verificando permisos
                Log.e("Permisos", "otorgados");
                TimerTask task = new TimerTask() {

                    @Override
                    public void run() {
                        checkSession();
                    }
                };
                Timer timer = new Timer();
                timer.schedule(task, SPLASH_SCREEN_DELAY);
            }//onGranted

            @Override
            public void onDenied(Context context, ArrayList<String> deniedPermissions) {
                // permission denied, block the feature.
                Log.e("Permisos", "Denegados");
                Intent i = new Intent(SplashScreen.this, SplashScreen.class);
                startActivity(i);
                SplashScreen.this.finish();

            }//onDenied
        });
    }//permisos

    public void checkSession(){
        if (sessionManager.getUserCredentials().length() <= 0){
            Intent mainIntent = new Intent().setClass(SplashScreen.this,
                    LoginActivity.class);
            startActivity(mainIntent);
            finish();
        }else{
            Intent mainIntent = new Intent().setClass(SplashScreen.this,
                    MainActivity.class);
            startActivity(mainIntent);
            finish();
        }
    }//CheckSession
}//Main
