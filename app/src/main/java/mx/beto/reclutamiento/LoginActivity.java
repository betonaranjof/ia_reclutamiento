package mx.beto.reclutamiento;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crowdfire.cfalertdialog.CFAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.beto.configs.SessionManager;

public class LoginActivity extends Activity implements View.OnClickListener {

    //Variables
    public EditText et_email, et_password;
    public Button btn_login;
    public TextView no_account;
    public String usuario, passw;
    public ProgressBar progressBar;
    public SessionManager sessionManager;
    public JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        sessionManager = new SessionManager(this);
        initElements();
    }//OnCreate

    public void initElements() {
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        no_account = findViewById(R.id.link_signup);
        btn_login = findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);
        progressBar = findViewById(R.id.progressbar);
        progressBar.setVisibility(View.GONE);
    }//initElements


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login:
                usuario = et_email.getText().toString().trim();
                passw = et_password.getText().toString().trim();
                if (usuario.isEmpty() || usuario.length() <= 1) {
                    mensajeAlerta(getResources().getString(R.string.empty_field), getResources().getString(R.string.empty_user));
                } else if (passw.isEmpty() || passw.length() <= 1) {
                    mensajeAlerta(getResources().getString(R.string.empty_field), getResources().getString(R.string.empty_password));
                } else {
                    requestLogin(usuario, passw);
                    progressBar.setVisibility(View.VISIBLE);
                }
                break;

            default:
                Log.e("Click", "unknow");
                break;
        }//switch
    }//onClick

    public void mensajeAlerta(String titulo, String mensaje) {
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                .setTitle(titulo)
                .setMessage(mensaje)
                .setTextGravity(Gravity.CENTER_HORIZONTAL)
                .setCancelable(false)
                .addButton("CERRAR", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE,
                        CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, (dialog, which) -> {
                            dialog.dismiss();
                        });

// Show the alert
        builder.show();
    }//MensajeAlerta

    public void requestLogin(String usr, String psw) {

        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST,
                "https://stage-api.cinepolis.com/v2/oauth/token",
                response -> {
            Log.e("respuesta", response);
                    try{
                        jsonObject = new JSONObject(response);
                        sessionManager.createSessionPatrol(
                                usr,
                                psw,
                                jsonObject.optString("access_token"),
                                jsonObject.optString("token_type")
                        );
                    }catch(JSONException e){
                        e.printStackTrace();
                    }
                    progressBar.setVisibility(View.GONE);
                    Intent mainAct = new Intent(this, MainActivity.class);
                    startActivity(mainAct);
                    this.finish();
                    },
                error -> {
            progressBar.setVisibility(View.GONE);
            mensajeAlerta("Wrong", "Verifique sus cuentas.");
            Log.e("VOLLEY Error", error.toString());
            try {
                NetworkResponse networkResponse = error.networkResponse;
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    Log.e("Error", "Error");
                }
                String flag = String.valueOf(networkResponse.statusCode);
                if (flag == null || flag.isEmpty()) {
                    Log.e("Flag", flag);
                } else {
                    Log.e("statusCode Error", "" + networkResponse.statusCode);
                    Log.e("Flag", "" + networkResponse.statusCode);
                }
            } catch (NullPointerException npe) {
                npe.printStackTrace();
                Log.e("NPE", npe.getMessage());
            }
        }//errorListener
        ) {
            /**
             * Credenciales:
             * pruebas_beto_ia@yahoo.com
             * Pruebas01
             * */
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("country_code", "MX");
                params.put("username", usr);
                params.put("password", psw);
                params.put("grant_type", "password");
                params.put("client_id", "IATestCandidate");
                params.put("client_secret", "c840457e777b4fee9b510fbcd4985b68");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> params = new HashMap<>();
                params.put("api_key", "stage_HNYh3RaK_Test");
                return params;
            }
        };
        mRequestQueue.add(jsonObjectRequest);

    }//requestLogin

}//Main
