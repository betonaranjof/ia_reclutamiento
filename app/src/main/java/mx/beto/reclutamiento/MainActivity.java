package mx.beto.reclutamiento;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.ss.bottomnavigation.BottomNavigation;

import mx.beto.reclutamiento.fragmentos.Cartelera;
import mx.beto.reclutamiento.fragmentos.ComplejoFragment;
import mx.beto.reclutamiento.fragmentos.Tarjeta;
import mx.beto.reclutamiento.fragmentos.UserProfile;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        initElements();
    }//onCreate

    public void initElements(){

        BottomNavigation bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setDefaultItem(0);
        bottomNavigation.setOnSelectedItemChangeListener(itemId -> {
            switch (itemId){
                case R.id.tab_profile:
                    transaction=getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_fragment_containers,new UserProfile());
                    break;
                case R.id.tab_card:
                    transaction=getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_fragment_containers,new Tarjeta());
                    break;
                case R.id.tab_movies:
                    transaction=getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_fragment_containers,new Cartelera());
                    break;
                case R.id.tab_complejos:
                    transaction=getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_fragment_containers,new ComplejoFragment());
                    break;
            }//switch
            transaction.commit();
        });
    }//initELements

    @Override
    public void onClick(View v) {
    }//onClick

}
